<?php

namespace Drupal\entity_hierarchy_field\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_hierarchy_field\Plugin\Field\FieldType\EntityHierarchyFieldType;

/**
 * Class HierarchyManager.
 *
 * @package Drupal\entity_hierarchy\Service
 */
class HierarchyManager {

  /**
   * Nom du service
   *
   * @const string
   */
  const SERVICE_NAME = 'entity_hierarchy.manager';

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Entity Type Manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Database
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Definition cache.
   *
   * @var array
   */
  protected $cache = [];

  /**
   * HierarchyManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   */
  public function __construct(\Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, Connection $database) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->database = $database;
  }

  /**
   * Return the hierarchy.
   *
   * @param EntityInterface $entity
   *   The entity.
   *
   * @return array|null
   *   The hierarchy.
   */
  public function getAllHierarchyFromEntity(EntityInterface $entity) {
    return $this->getAllHierarchyByEntityTypeAndBundle($entity->getEntityTypeId(), $entity->bundle());

  }

  /**
   * Return the hierarchy.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return array|null
   *   The hierarchy.
   */
  public function getAllHierarchyByEntityTypeAndBundle($entityTypeId, $bundle) {
    $hierarchy = [];
    if ($fieldName = $this->getHierarchyFieldNameFromEntityTypeAndBundle($entityTypeId, $bundle)) {
      $flatHierarchyData = $this->getFlatHierarchyData($entityTypeId, $bundle, $fieldName);
      $hierarchy = $this->getHierarchyFromFlatData($flatHierarchyData, $entityTypeId);
    }

    return $hierarchy;
  }

  /**
   * Return the hierarchy field of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return EntityHierarchyFieldType|null
   *   The field or null if no hierarchy field is associated.
   */
  public function getHierachyFieldFromEntity(EntityInterface $entity) {
    if ($fieldName = $this->getHierarchyFieldNameFromEntityTypeAndBundle($entity->getEntityTypeId(), $entity->bundle())) {
      return $entity->get($fieldName);
    }
    return NULL;
  }

  /**
   * Return the hierarchy field name.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return int|string|null
   *   The name of the hierarchy field if exists.
   */
  public function getHierarchyFieldNameFromEntityTypeAndBundle($entityTypeId, $bundle) {
    if (!array_key_exists($entityTypeId, $this->cache)) {
      $this->cache[$entityTypeId] = [];
    }

    if (!array_key_exists($bundle, $this->cache[$entityTypeId])) {
      $allFieldData = $this->entityFieldManager->getFieldMapByFieldType(EntityHierarchyFieldType::TYPE_ID);
      if (array_key_exists($entityTypeId, $allFieldData)) {
        foreach ($allFieldData[$entityTypeId] as $fieldName => $fieldData) {
          if (in_array($bundle, $fieldData['bundles'])) {
            $this->cache[$entityTypeId][$bundle] = $fieldName;
            return $fieldName;
          }
        }
      }
      $this->cache[$entityTypeId][$bundle] = NULL;
    }

    return $this->cache[$entityTypeId][$bundle];
  }

  /**
   * Return the flat data of hierarchy between start and end position.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   * @param string $fieldName
   *   The field.
   * @param int $start
   *   The start position of the hierarchy
   * @param int $end
   *   The end position of the hierarchy
   */
  protected function getFlatHierarchyData($entityTypeId, $bundle, $fieldName, $start = NULL, $end = NULL) {
    $tableName = $entityTypeId . '__' . $fieldName;
    $query = $this->database->select($tableName, 't');


    $query->addField('t', 'entity_id', 'id');
    $query->addField('t', $fieldName . '_' . EntityHierarchyFieldType::FIELD_POSITION, EntityHierarchyFieldType::FIELD_POSITION);
    $query->addField('t', $fieldName . '_' . EntityHierarchyFieldType::FIELD_SIZE, EntityHierarchyFieldType::FIELD_SIZE);
    $query->addField('t', $fieldName . '_' . EntityHierarchyFieldType::FIELD_LEVEL, EntityHierarchyFieldType::FIELD_LEVEL);

    $query->condition('bundle', $bundle);
    $query->range($start, $end);
    $query->orderBy($fieldName . '_' . EntityHierarchyFieldType::FIELD_POSITION, 'ASC');

    $result = $query->execute()->fetchAll();
    return $result;
  }

  /**
   * Return the hierarchy from the flat Hierarchy Data.
   *
   * @param array $flatHierarchyData
   *   The flat data.
   * @param string $entityTypeId
   *   The entity type id.
   */
  protected function getHierarchyFromFlatData(array $flatHierarchyData, $entityTypeId) {
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);

    // Parse the flat data.
    $relativeLevel = 0;
    $parents = [];
    foreach ($flatHierarchyData as $flatData) {
      $entity = $entityStorage->load($flatData['id']);

      // @todo : Init entity.
    }
  }

}