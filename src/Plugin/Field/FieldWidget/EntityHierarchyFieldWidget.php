<?php

namespace Drupal\entity_hierarchy_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_hierarchy_field\Service\HierarchyManager;

/**
 * Defines the 'entity_hierarchy_field_widget' field widget.
 *
 * @FieldWidget(
 *   id = "entity_hierarchy_field_widget",
 *   label = @Translation("Entity Hierarchy Field Widget"),
 *   field_types = {"entity_hierarchy_type"},
 * )
 */
class EntityHierarchyFieldWidget extends EntityReferenceAutocompleteWidget {

  /**
   * Widget id
   *
   * @const string
   */
  const WIDGET_ID = 'entity_hierarchy_field_widget';

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity = $items->getParent()->getEntity();
    dump( HierarchyManager::me()->getAllHierarchyFromEntity($entity));exit;
    return parent::formElement($items, $delta, $element, $form, $form_state);
  }

}
