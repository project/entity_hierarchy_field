<?php

namespace Drupal\entity_hierarchy_field\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'entity_hierarchy_type' field type.
 *
 * @FieldType(
 *   id = "entity_hierarchy_type",
 *   label = @Translation("Entity Hierarchy"),
 *   category = @Translation("General"),
 *   default_widget = "entity_hierarchy_field_widget",
 *   default_formatter = "string"
 * )
 *
 * @DCG
 * If you are implementing a single value field type you may want to inherit
 * this class form some of the field type classes provided by Drupal core.
 * Check out /core/lib/Drupal/Core/Field/Plugin/Field/FieldType directory for a
 * list of available field type implementations.
 */
class EntityHierarchyFieldType extends FieldItemBase {

  /**
   * Field type id
   *
   * @const string
   */
  const TYPE_ID = 'entity_hierarchy_type';

  /**
   * Position
   *
   * @const int
   */
  const FIELD_POSITION = 'position';

  /**
   * Size
   *
   * @const int
   */
  const FIELD_SIZE = 'size';

  /**
   * Level
   *
   * @const int
   */
  const FIELD_LEVEL = 'level';

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties[static::FIELD_POSITION] = DataDefinition::create('integer');
    $properties[static::FIELD_SIZE] = DataDefinition::create('integer');
    $properties[static::FIELD_LEVEL] = DataDefinition::create('integer');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      static::FIELD_POSITION => [
        'type' => 'int',
        'default' => 0,
      ],
      static::FIELD_SIZE => [
        'type' => 'int',
        'default' => 0,
      ],
      static::FIELD_LEVEL => [
        'type' => 'int',
        'default' => 0,
      ]
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

}
